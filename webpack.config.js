const path = require("path");

module.exports = {
  entry: path.resolve(__dirname, "src/main.ts"),
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist")
  },
  mode: "development",
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, "src"),
        loader: "babel-loader",
        query: {
          presets: ["es2015"]
        }
      },
      {
        test: /\.ts$/,
        loader: "ts-loader",
        exclude: "/node_modules/"
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader: "file-loader",
        options: {
          limit: 15000,
          name: "[name].[ext]"
        }
      }
    ]
  },
  resolve: {
    modules: ["node_modules", "src", "images"],
    extensions: [".js", ".ts"]
  },
  devtool: "source-map",
  devServer: {
    historyApiFallback: true,
    contentBase: "./"
  }
};
