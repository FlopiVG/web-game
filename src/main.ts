import WebGame from "./core/index";
import MainMenu from "./stages/MainMenu";
import Game from "./stages/Game";
import Rain from "./stages/Rain";
import Test from "./stages/Test";

new WebGame({
  Stages: [MainMenu, Rain, Test, Game]
});
