import Marta from "../actors/Marta";
import Enemy from "../actors/Enemy";

export default {
  id: "Game",
  debugMode: true,
  scenario: [
    [1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 1],
    [1, 0, 2, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 1],
    [1, 2, 0, 0, 0, 1, 1],
    [1, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 2, 0, 1, 1],
    [1, 2, 0, 0, 2, 0, 1],
    [1, 1, 1, 1, 1, 1, 1]
  ],
  init() {
    this.add({
      type: "player",
      component: Marta,
      x: 40,
      y: 40,
      id: "marta"
    });
    this.add({
      type: "actor",
      component: Enemy,
      x: 80,
      y: 80,
      id: "enemy1"
    });

    console.log(`Starting stage: ${this.id}`);
  }
};
