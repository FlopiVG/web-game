import Gota from "../actors/Gota";
import CONSTANT from "../core/constant";

export default {
  id: "rain",
  init() {
    console.log("Rain initialize");
  },
  update() {
    this.add({
      type: "entity",
      component: Gota,
      id: Math.random(),
      x: Math.random() * CONSTANT.CANVAS_WIDTH,
      spdY: Math.random() * 20 + 5
    });

    this.Entities.filter(entity => entity.type === "gota").forEach(entity => {
      if (entity.y >= CONSTANT.CANVAS_HEIGHT) this.removeEntity(entity);
    });
  }
};
