import Enemy from "actors/Enemy";
import Gota from "../actors/Gota";

export default {
  id: "test",

  init() {
    console.log("Test initialize");
    this.add({ type: "entity", id: "Gota", component: Gota });
  }
};
