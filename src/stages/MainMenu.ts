import Button from "widgets/Button";

export default {
  id: "MainMenu",
  init() {
    this.add({
      component: Button,
      type: "widget",
      x: 240,
      y: 200,
      id: "start-game",
      text: "Start Game",
      handleClick: () => {
        this.changeState("Game");
      }
    });

    console.log(`Starting stage: ${this.id}`);
  }
};
