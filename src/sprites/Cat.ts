import Canvas from "core/canvas";
import Actor from "core/entities/Actor";

class Cat extends Actor {
  private status: string;
  private images: any;
  private count: number;

  constructor(props) {
    super(props);

    this.x = props.x;
    this.y = props.y;
    this.status = "idle";
    this.images = {};
    this.count = 1;
    this.ctx.canvas.addEventListener("keydown", e => {
      if (e.keyCode === 68 || e.keyCode === 100) {
        this.moveRight();
      }
    });
    this.ctx.canvas.addEventListener("keyup", e => (this.status = "idle"));
    this.preload();
  }
  preload() {
    let imgToLoad = 0;
    let imgLoaded = 0;

    this.images.idle = [];
    for (let i = 1; i <= 10; i++) {
      imgToLoad += 1;
      let img = new Image();
      img.src = `./images/cat/Idle (${i}).png`;
      img.onload = () => {
        imgLoaded += 1;
        if (imgToLoad === imgLoaded) {
          this.initialize();
        }
      };
      this.images.idle.push(img);
    }

    this.images.walk = [];
    for (let i = 1; i <= 10; i++) {
      imgToLoad += 1;
      let img = new Image();
      img.src = `./images/cat/Walk (${i}).png`;
      img.onload = () => {
        imgLoaded += 1;
        if (imgToLoad === imgLoaded) {
          this.initialize();
        }
      };
      this.images.walk.push(img);
    }
  }
  initialize() {}
  update(delta) {
    super._update(delta);
    this.setIdle();
  }
  setIdle() {
    this.drawInCanvas(() => {
      this.ctx.drawImage(
        this.images[this.status][this.count],
        this.x,
        this.y,
        64,
        64
      );
    });
    this.count++;
    this.count = this.count >= 10 ? 1 : this.count;
  }
  moveRight() {
    this.status = "walk";
    this.x = this.x + 10;
  }
}

export default Cat;
