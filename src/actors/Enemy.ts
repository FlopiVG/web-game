export default {
  type: "Enemy",
  color: "#4C9900",
  isRigid: true,
  emoji: "👾",
  debugMode: true,
  update() {
    this.x++;
  }
};
