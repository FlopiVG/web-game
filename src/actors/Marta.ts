export default {
  type: "Player",
  isRigid: true,
  emoji: "👩",
  debugMode: true
};
