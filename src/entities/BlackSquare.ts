export default {
  emoji: "🔲",
  isRigid: true,
  immovable: true
};
