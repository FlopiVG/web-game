import Canvas from "./canvas";
import CONSTANT from "./constant";
import StageManager from "./StageManager";
import DebugScreen from "./debugScreen";
import General from "./General";
import Actor from "./entities/Actor";
import Player from "./entities/Player";
import Widget from "./Widget";
import Entity from "./entities/Entity";
import BlackSquare from "../entities/BlackSquare";
import WhiteSquare from "../entities/WhiteSquare";

class Stage extends General {
  public Entities: any[] = [];
  public id: string;
  private lastFrameTimeMs: number;
  private loop: boolean;
  public scenario: number[][];

  constructor(props) {
    super(props);

    this.id = props.id;
    this.lastFrameTimeMs = 0;
    this.loop = false;
  }
  _update(timestamp = 0) {
    if (!this.loop) return;

    if (timestamp < this.lastFrameTimeMs + 1000 / CONSTANT.MAXFPS) {
      requestAnimationFrame(this._update.bind(this));
      return;
    }
    let delta = timestamp - this.lastFrameTimeMs;

    this.lastFrameTimeMs = timestamp;
    Canvas.cleanCanvas();

    this.Entities.forEach(entity => {
      // Interact with other entities
      this.Entities.filter(item => item.id !== entity.id).forEach(item =>
        entity.interact(item)
      );
      entity._update(delta);
    });

    this.debugMode && DebugScreen.drawStageDebug(this);
    this.update();
    requestAnimationFrame(this._update.bind(this));
  }

  update() {}

  init() {}

  start() {
    if (this.scenario) this.buildScenario();
    this.init();
    this.loop = true;
    requestAnimationFrame(this._update.bind(this));
  }

  add({ component, type, ...rest }) {
    switch (type.toUpperCase()) {
      case "ENTITY":
        this.Entities.push(Object.assign(new Entity(rest), component));
        break;
      case "ACTOR":
        this.Entities.push(Object.assign(new Actor(rest), component));
        break;
      case "PLAYER":
        this.Entities.push(Object.assign(new Player(rest), component));
        break;
      case "WIDGET":
        this.Entities.push(Object.assign(new Widget(rest), component));
        break;
      default:
        throw new Error(`Dont found Entity with type ${type}`);
    }
  }

  removeEntity(entity) {
    this.Entities = this.Entities.filter(item => item.id !== entity.id);
  }

  changeState(state) {
    StageManager.changeStage(this.id, state);
  }
  destroy() {
    this.Entities = [];
    this.loop = false;
    this.debug(false);
    DebugScreen.cleanDebugScreen();
  }

  buildScenario() {
    this.ctx.canvas.width = this.scenario[0].length * 32;
    this.ctx.canvas.height = this.scenario.length * 32;

    this.scenario.forEach((row, i) => {
      row.forEach((cell, j) => {
        if (cell === 1) {
          this.add({
            component: BlackSquare,
            type: "entity",
            x: j * 32,
            y: i * 32,
            id: `block-${i}-${j}`
          });
        } else if (cell === 2) {
          this.add({
            component: WhiteSquare,
            type: "entity",
            x: i * 32,
            y: j * 32,
            id: `block-${i}-${j}`
          });
        }
      });
    });
  }
}

export default Stage;
