import CONSTANT from "core/constant";

class Canvas {
  private static _instance: Canvas = new Canvas();
  private canvas: CanvasRenderingContext2D;

  constructor() {
    if (Canvas._instance) {
      throw new Error("Error: Instantiation failed");
    }
    this.canvas = this.createCanvas();
    Canvas._instance = this;
  }

  public static getInstance(): Canvas {
    return Canvas._instance;
  }

  private createCanvas() {
    const $canvas = document.createElement("canvas");
    $canvas.id = "ctx";
    $canvas.height = CONSTANT.CANVAS_HEIGHT;
    $canvas.width = CONSTANT.CANVAS_WIDTH;
    $canvas.style.border = CONSTANT.CANVAS_BORDER;
    $canvas.tabIndex = 1;
    document.querySelector("#main").appendChild($canvas);
    $canvas.focus();
    return $canvas.getContext("2d");
  }

  public getCanvas(): CanvasRenderingContext2D {
    return this.canvas;
  }

  public cleanCanvas(): void {
    this.canvas.clearRect(0, 0, CONSTANT.CANVAS_WIDTH, CONSTANT.CANVAS_HEIGHT);
  }
}

export default Canvas.getInstance();
