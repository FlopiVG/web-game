import Stage from "./Stage";

class StageManager {
  private static _instance: StageManager = new StageManager();
  private stageItems: Stage[] = [];

  constructor() {
    if (StageManager._instance) {
      throw new Error("Error: Instantiation failed");
    }
    StageManager._instance = this;
  }

  public static getInstance(): StageManager {
    return StageManager._instance;
  }

  public createStageManager(stages): void {
    stages.forEach(stage => {
      const parentStage = new Stage({ id: stage.id });
      this.stageItems.push(Object.assign(parentStage, stage));
    });
  }

  public getStages(): Stage[] {
    return this.stageItems;
  }

  public changeStage(idCurrentStage, idNextStage): void {
    if (idCurrentStage) {
      this.stageItems.filter(stage => stage.id === idCurrentStage)[0].destroy();
    }

    const findNextStage = this.stageItems.filter(
      stage => stage.id === idNextStage
    )[0];

    if (findNextStage) findNextStage.start();
    else throw new Error(`Invalid Stage with id ${idNextStage}`);
  }

  public initFirstScene(): void {
    if (this.stageItems.length) {
      this.stageItems[0].start();
    } else {
      throw new Error("There are no Stages.");
    }
  }
}

export default StageManager.getInstance();
