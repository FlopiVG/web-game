import CONSTANT from "core/constant";

class DebugScreen {
  private static _instance: DebugScreen = new DebugScreen();
  private $debug: HTMLDivElement;

  constructor() {
    if (DebugScreen._instance) {
      throw new Error("Error: Instantiation failed");
    }

    this.$debug = this.createDebugScreen();

    DebugScreen._instance = this;
  }

  public static getInstance(): DebugScreen {
    return DebugScreen._instance;
  }

  createDebugScreen(): HTMLDivElement {
    const $debug = document.createElement("div");
    $debug.id = "debugScreen";

    $debug.style.cssFloat = "left";
    $debug.style.marginLeft = "20px";
    $debug.style.padding = "20px";

    document.querySelector("body").appendChild($debug);
    return $debug;
  }

  getDebugScreen(): HTMLDivElement {
    return this.$debug;
  }

  drawVariables(id, entityId, variables) {
    if (
      Object.values(variables).filter(variable => variable === undefined)
        .length !== 0
    )
      return;

    const newDiv = !document.getElementById(id);
    const $element =
      document.getElementById(`${id}-${entityId}`) ||
      document.createElement("div");
    if (newDiv) $element.id = `${id}-${entityId}`;

    let text = "";
    Object.keys(variables).forEach(key => {
      text += `<strong>${key}:</strong> ${variables[key]} `;
    });
    $element.innerHTML = text;

    this.$debug.appendChild($element);
  }

  drawTitle(type, id, el) {
    const $elem = document.createElement("h5");
    $elem.innerHTML = `<center><strong>${type} ${id}</strong></center>`;
    el.appendChild($elem);
  }

  drawEntityDebug(entity) {
    const $divEntity =
      document.getElementById(entity.id) || document.createElement("div");
    $divEntity.innerHTML = "";
    $divEntity.id = entity.id;

    // Set title
    this.drawTitle("Entity", entity.id, $divEntity);

    // SET CONTENT
    this.drawVariables("title", entity.id, {
      Entity: entity.id,
      Type: entity.type
    });
    this.drawVariables("position", entity.id, {
      ["Position X"]: Math.floor(entity.x),
      ["Position Y"]: Math.floor(entity.y)
    });
    this.drawVariables("speed", entity.id, {
      spdX: entity.spdX,
      spdY: entity.spdY
    });
    this.drawVariables("countAnim", entity.id, {
      ["Count Animation"]: entity.countAnim
    });

    $divEntity.appendChild(document.createElement("hr"));
    this.$debug.appendChild($divEntity);
  }

  drawStageDebug(stage) {
    const $divStage =
      document.getElementById(stage.id) || document.createElement("div");
    $divStage.innerHTML = "";
    $divStage.id = stage.id;

    // Set title
    this.drawTitle("Stage", stage.id, $divStage);

    this.drawVariables("entities", stage.id, {
      ["Entity length"]: stage.Entities.length
    });

    $divStage.appendChild(document.createElement("hr"));
    this.$debug.appendChild($divStage);
  }

  cleanDebugScreen() {
    document.getElementById("debugScreen").innerHTML = "";
  }
}

export default DebugScreen.getInstance();
