import Canvas from "core/canvas";

class Animation {
  private ctx: CanvasRenderingContext2D;
  private id: string;
  private width: number;
  private height: number;
  private animLength: number;
  private _animLength: number;
  private x: number;
  private y: number;
  private animFrame: number;
  private animSpeed: number;
  private countAnim: number;

  constructor(props) {
    this.ctx = Canvas.getCanvas();
    // Set variables
    this.id = props.id;
    this.width = props.width;
    this.height = props.height;
    this.x = props.x || 0;
    this.y = props.y * this.height || 0;

    this.animLength = props.animLength;
    this._animLength = props.animLength;
    this.animFrame = 0;
    this.animSpeed = 5;
    this.countAnim = 0;
  }
  update(delta) {
    if (this.animFrame >= this.animLength) this.animFrame = 0;
    if (this.countAnim >= this.animSpeed) {
      this.animFrame++;
      this.countAnim = 0;
      this.x = this.width * this.animFrame;
    }
    this.countAnim++;
  }
}

export default Animation;
