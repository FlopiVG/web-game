import StageManager from "./StageManager";

class WebGame {
  constructor(props) {
    console.log("Create Game");
    StageManager.createStageManager(props.Stages);
    StageManager.initFirstScene();
  }
}

export default WebGame;
