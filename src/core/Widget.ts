import Entity from "./entities/Entity";

class Widget extends Entity {
  public id: string;
  public x: number;
  public y: number;
  public width: number;
  public height: number;
  private mouseX: number;
  private mouseY: number;

  public text: string;

  private bg: string;
  private hover: string;
  private active: string;

  private _active: boolean;

  constructor(props) {
    super(props);

    this.x = props.x;
    this.y = props.y;
    this.width = props.width;
    this.height = props.height;

    this._active = false;

    this.text = props.text;

    // Styles
    this.bg = props.bg || "black";
    this.hover = props.hover || "grey";
    this.active = props.active || props.bg || "black";

    this.handleClick = props.handleClick || this.handleClick;

    // Event handlers
    this.ctx.canvas.addEventListener("mousemove", e => {
      this.mouseX = e.clientX - 10;
      this.mouseY = e.clientY - 10;
    });
    this.ctx.canvas.addEventListener("mousedown", e => {
      this._active = true;
    });
    this.ctx.canvas.addEventListener("mouseup", e => {
      this._active = false;
      if (this.isHover()) {
        this.handleClick();
      }
    });
  }

  draw() {
    this.ctx.fillStyle = this.bg;
    if (this.isHover()) {
      this.ctx.fillStyle = this.hover;
    }
    if (this._active && this.isHover()) {
      this.ctx.fillStyle = this.active;
    }
    this.ctx.fillRect(this.x, this.y, this.width, this.height);

    if (this.text) this.setText();
  }

  setText() {
    this.drawInCanvas(() => {
      this.ctx.fillStyle = "#FFF";
      this.ctx.textAlign = "center";
      this.ctx.fillText(
        this.text,
        this.x + this.width / 2,
        this.y + this.height / 2
      );
    });
  }

  isHover() {
    return (
      this.x < this.mouseX &&
      this.x + this.width > this.mouseX &&
      this.y < this.mouseY &&
      this.y + this.height > this.mouseY
    );
  }
  handleClick() {
    console.log("Don't specifies handle click");
  }
  drawInCanvas(func) {
    this.ctx.save();
    func();
    this.ctx.restore();
  }
}

export default Widget;
