import Canvas from "../core/canvas";

class General {
  public id: string;
  public debugMode: boolean;
  public ctx: CanvasRenderingContext2D;

  constructor(props) {
    // Variables
    this.id =
      props.id ||
      this.throwError(
        `You must provide an id in ${this.constructor.name} element`
      );

    // Get canvas instance
    this.ctx = Canvas.getCanvas();

    // To draw debug mode
    this.debugMode = false;
  }
  drawInCanvas(func) {
    this.ctx.save();
    func();
    this.ctx.restore();
  }
  debug(bool: boolean) {
    this.debugMode = bool;
  }

  throwError(msg) {
    throw msg;
  }
}

export default General;
