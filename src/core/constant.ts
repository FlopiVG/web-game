export default {
  // CANVAS STYLE
  CANVAS_HEIGHT: 480,
  CANVAS_WIDTH: 640,
  CANVAS_BORDER: 'solid 1px #000',

  MAXFPS: 60,

  // CONTROLS
  UP: 87,
  DOWN: 83,
  LEFT: 65,
  RIGHT: 68
};