import Actor from "core/entities/Actor";
import CONSTANT from "core/constant";

class Player extends Actor {
  private speed: number;
  public animType: string;

  constructor(props) {
    super(props);
    // Set variables
    this.type = "Player";
    this.color = "#FF3333";
    this.speed = 10;

    this.animType = "idle";

    this.setControls();
  }
  setControls() {
    this.ctx.canvas.addEventListener("keydown", e => {
      if (e.keyCode === CONSTANT.UP) {
        this.spdY = -this.speed;
        this.animType = "moveUp";
      }
      if (e.keyCode === CONSTANT.DOWN) {
        this.spdY = this.speed;
        this.animType = "moveDown";
      }
      if (e.keyCode === CONSTANT.RIGHT) {
        this.spdX = this.speed;
        this.animType = "moveRight";
      }
      if (e.keyCode === CONSTANT.LEFT) {
        this.spdX = -this.speed;
        this.animType = "moveLeft";
      }
    });
    this.ctx.canvas.addEventListener("keyup", e => {
      if (e.keyCode === CONSTANT.UP || e.keyCode === CONSTANT.DOWN) {
        this.spdY = 0;
        if (!this.spdX && !this.spdY) this.animType = "idle";
      }
      if (e.keyCode === CONSTANT.RIGHT || e.keyCode === CONSTANT.LEFT) {
        this.spdX = 0;
        if (!this.spdX && !this.spdY) this.animType = "idle";
      }
    });
  }
}

export default Player;
