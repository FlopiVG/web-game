import Entity from "core/entities/Entity";

class Actor extends Entity {
  public spdX: number;
  public spdY: number;

  constructor(props) {
    super(props);
    // Set variables
    this.spdX = props.spdX | 0;
    this.spdY = props.spdY | 0;
  }
  _update(delta) {
    super._update(delta);
    this.move(delta);
  }
  move(delta) {
    this.x += this.spdX * delta * 0.01;
    this.y += this.spdY * delta * 0.01;
  }
}

export default Actor;
