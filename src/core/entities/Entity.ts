import DebugScreen from "../debugScreen";
import Animation from "../Animation";
import General from "../General";

class Entity extends General {
  public type: string;
  public x: number;
  public y: number;
  public width: number;
  public height: number;
  public cWidth: number;
  public cHeight: number;
  public cX: number;
  public cY: number;
  public color: string;
  public animList: any;
  public isRigid: boolean;
  public emoji: string;
  public immovable: boolean;

  constructor(props) {
    super(props);
    // Set variables
    this.type = "Entity";
    this.x = props.x;
    this.y = props.y;
    this.width = props.width || 32;
    this.height = props.width || 32;

    // Collisions
    this.isRigid = props.isRigid || false;
    this.immovable = props.immovable || false;
    this.cWidth = this.width;
    this.cHeight = this.height;
    this.cX = 0;
    this.cY = 0;

    this.color = "#000";

    // Animations
    this.animList = {};
  }
  addAnimation(id, width, height, x, y, animLength) {
    this.animList[id] = new Animation({ id, width, height, x, y, animLength });
  }
  _update(delta) {
    this.update(delta);
    this._draw();
    this.debugMode && this.drawDebug();
  }

  _draw() {
    this.drawInCanvas(() => {
      this.draw();
    });
  }

  update(delta) {}

  draw() {
    if (this.emoji) {
      this.ctx.font = `${this.width}px sans-serif`;
      this.ctx.fillText(this.emoji, this.x, this.y + this.width - 5);
    } else {
      this.ctx.fillStyle = this.color;
      this.ctx.fillRect(this.x, this.y, this.width, this.height);
    }
  }
  drawDebug() {
    this.drawInCanvas(() => {
      this.ctx.strokeRect(
        this.x + this.cX,
        this.y + this.cY,
        this.cWidth,
        this.cHeight
      );
    });
    DebugScreen.drawEntityDebug(this);
  }
  interact(entity) {
    // if (entity.id === "marta" && this.id === "block-0-0") debugger;
    if (this.isRigid && entity.isRigid && !this.immovable)
      this.collisionSide(entity);
  }
  collisionSide(entity) {
    let vX =
        this.x +
        this.cX +
        this.cWidth / 2 -
        (entity.x + entity.cX + entity.cWidth / 2),
      vY =
        this.y +
        this.cY +
        this.cHeight / 2 -
        (entity.y + entity.cY + entity.cHeight / 2),
      hWidths = this.cWidth / 2 + entity.cWidth / 2,
      hHeights = this.cHeight / 2 + entity.cHeight / 2,
      colDir = null;

    if (Math.abs(vX) < hWidths && Math.abs(vY) < hHeights) {
      let oX = hWidths - Math.abs(vX),
        oY = hHeights - Math.abs(vY);
      if (oX >= oY) {
        if (vY > 0) {
          colDir = "t";
          this.y += oY;
        } else {
          colDir = "b";
          this.y -= oY;
        }
      } else {
        if (vX > 0) {
          colDir = "l";
          this.x += oX;
        } else {
          colDir = "r";
          this.x -= oX;
        }
      }
    }
    return colDir;
  }
  getDistance(entity) {
    return Math.sqrt(
      Math.pow(
        this.x +
          this.cX +
          this.cWidth / 2 -
          (entity.x + entity.cX + entity.cWidth / 2),
        2
      ) +
        Math.pow(
          this.y +
            this.cY +
            this.cHeight / 2 -
            (entity.y + entity.cY + entity.cHeight / 2),
          2
        )
    );
  }

  debug(bool) {
    this.debugMode = bool;
  }

  drawInCanvas(func) {
    this.ctx.save();
    func();
    this.ctx.restore();
  }
}

export default Entity;
